package fishbowl;

import java.io.File
import scalaz._
import scalaz.stream._
import scalaz.concurrent.Task

trait MonthlyArchiveOps extends RenderOps {
	def mainArchive(root: File, blog: Blog): Process[Task, FileOp] =
		Process.emit(
			WriteString(
				html.archives(blog.months, blog).body, 
				new File(root, "archives/index.html"))) 	

	def monthlyArchives(root: File, blog: Blog): Process[Task, FileOp] = for {
		m <- Process.emitAll(blog.months)
		ops <- monthlyArchive(root, blog, m)
	} yield (ops)

	def monthlyArchive(root: File, blog: Blog, month: MonthlyArchive) = 
		Process.await(loadPosts(month.posts)) { ps =>
			Process.emit(WriteString(
				html.month(blog, month, ps).body,
				month.filePath(root)))
		}
}

object monthlyArchives extends MonthlyArchiveOps