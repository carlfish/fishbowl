package fishbowl

import java.io.File

object Main extends App {
  lazy val src = sys.env("FISHBOWL_HOME")
  lazy val dest = sys.env("FISHBOWL_OUT")

  fishbowl.build(src, dest).unsafePerformSync
}
