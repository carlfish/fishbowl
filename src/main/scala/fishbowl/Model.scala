package fishbowl;

import java.time._
import java.time.format._
import java.io.File

case class Post(
	title: String,
	author: String,
	basename: String,
	srcdir: File,
	body: File,
	tags: Set[String],
	excerpt: Option[String],
	publishDate: ZonedDateTime,
	tag: Option[String])

case class BlogConfig(
	title: String,
	longtitle: String,
	subtitle: String,
	description: String,
	authors: Map[String, Author],
	uri: String,
	tag: String)

case class Author(
	displayName: String,
	uri: String)

trait MonthlyArchive {
	def posts: List[Post]

	def title: String

	def longtitle: String

	def uri: String

	def filePath(base: File): File

	def next: Option[MonthlyArchive]

	def previous: Option[MonthlyArchive]
}

trait Blog {
	def srcdir: File

	def posts: List[Post]

	def title: String

	def longtitle: String

	def subtitle: String

	def description: String

	def uri: String

	def author(username: String): Option[Author]

	def uriOf(post: Post): String

	def filePathOf(base: File, post: Post): File

	def next(post: Post): Option[Post]

	def previous(post: Post): Option[Post]

	def firstMonth: Option[MonthlyArchive]

	def latestMonth: Option[MonthlyArchive]

	def months: Stream[MonthlyArchive]

	def tag: String
}

object Blog {
	def apply(srcdir: File, config: BlogConfig, ps: Seq[Post]) = new BlogIndex(srcdir, config, ps)
}

class MonthIndex(blog: Blog, month: YearMonth) extends MonthlyArchive {
	lazy val posts = blog.posts.filter(p => YearMonth.from(p.publishDate) == month)

	lazy val title = DateTimeFormatter.ofPattern("MMMM yyyy").format(month)

	lazy val longtitle = title

	lazy val uri = DateTimeFormatter.ofPattern(s"'${blog.uri}/'yyyy/MM/").format(month)

	def filePath(base: File) = new File(base, DateTimeFormatter.ofPattern("yyyy/MM/'index.html'").format(month))

	def next = {
		blog.posts.reverse.find(p => YearMonth.from(p.publishDate).compareTo(month) > 0)
			.map(p1 => new MonthIndex(blog, YearMonth.from(p1.publishDate)))
	}

	def previous = {
		blog.posts.find(p => YearMonth.from(p.publishDate).compareTo(month) < 0)
			.map(p1 => new MonthIndex(blog, YearMonth.from(p1.publishDate)))
	}
}

class BlogIndex(basedir: File, config: BlogConfig, ps: Seq[Post]) extends Blog {
	// Scala isn't best at finding these automatically.
	implicit object ZonedDateTimeOrdering extends Ordering[ZonedDateTime] {
		  def compare(d1: ZonedDateTime, d2: ZonedDateTime) = d1.compareTo(d2)
	}

	lazy val posts = List() ++ ps.sortBy(_.publishDate).reverse

	override def srcdir = basedir
	override def title = config.title
	override def longtitle = config.longtitle
	override def subtitle = config.subtitle
	override def description = config.description
	override def uri = config.uri
	override def tag = config.tag
	override def author(username: String) = config.authors.get(username)

	override def uriOf(post: Post) = DateTimeFormatter.ofPattern(s"'$uri/'yyyy/MM/dd/'${post.basename}/'").format(post.publishDate)
	override def filePathOf(base: File, post: Post) = new File(base, DateTimeFormatter.ofPattern(s"yyyy/MM/dd/'${post.basename}/'").format(post.publishDate))

	override def next(post: Post) = {
		val i = posts.indexOf(post)

		if (i <= 0) None else Some(posts(i - 1))
	}

	override def previous(post: Post) = {
		val i = posts.indexOf(post)

		if (i == -1 || i == (posts.length - 1)) None else Some(posts(i + 1))
	}

	override def months = {
		def nextM(mo: Option[MonthlyArchive]): Stream[MonthlyArchive] = {
			mo.map(m => m #:: nextM(m.previous)).getOrElse(Stream.Empty)
		}

		nextM(latestMonth)
	}

	override def latestMonth = posts.headOption.map(p => new MonthIndex(this, YearMonth.from(p.publishDate)))

	override def firstMonth = posts.lastOption.map(p => new MonthIndex(this, YearMonth.from(p.publishDate)))
}
