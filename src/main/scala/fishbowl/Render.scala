package fishbowl;

import java.io.File
import java.time.format.DateTimeFormatter
import scalaz._
import scalaz.concurrent.Task
import scalaz.std.list._
import scalaz.stream._
import scalaz.syntax.traverse._

trait RenderOps {
	def loadPosts(posts: List[Post]): Task[List[(Post, String)]] =
		loadBodies(posts).map(posts zip _)

	def loadBody(post: Post): Task[String] =  files.loadFile(post.body)

	def loadBodies(posts: List[Post]): Task[List[String]] = posts.map(loadBody).sequence
}

trait DateOps {
	def month = fmt("MMMM")

	def shortMonth = fmt("MMM")

	def dayOfMonth(post: Post) = post.publishDate.getDayOfMonth

	def fullDate = fmt("MMMM d, yyyy")

	def fullDateTime = fmt("MMMM d, yyyy h:mm a")

	def shortYear = fmt("yyyy")

	def justTime = fmt("h:mm a")

	def isoDay = fmt("yyyy-MM-dd")

	def isoTime(post: Post) = DateTimeFormatter.ISO_INSTANT.format(post.publishDate)

	private def fmt(pat: String): Post => String = {
		val f = DateTimeFormatter.ofPattern(pat)
		(post: Post) => f.format(post.publishDate)
	}
}

object dates extends DateOps
