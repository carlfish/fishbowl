package fishbowl

import java.io.File
import scalaz._
import scalaz.concurrent.Task
import scalaz.stream._


trait BlogOps {
	implicit val unitReducer: Reducer[Unit, Unit] = Reducer.UnitReducer;

	val builders: List[Builder] = List(
			entryPages.entryPages _,
			indexPages.rootPage,
			indexPages.atomFeed,
			indexPages.siteMap,
			monthlyArchives.mainArchive _,
			monthlyArchives.monthlyArchives _,
			copyOverlay _
		)

	def build(src: String, dest: String): Task[Unit] = for {
		blog <- indexer.loadBlogIndex(new File(src))
		tasks = builders.map(runBuilder(new File(dest), blog))
		result <- Task.reduceUnordered(tasks)
	} yield (result)

	def runBuilder(root: File, blog: Blog)(builder: Builder): Task[Unit] =
		builder(root, blog).to(writeFiles).run

 	def writeFiles: Sink[Task, FileOp] =
 		sink.lift(files.perform)

	def copyOverlay(root: File, blog: Blog): Process[Task, FileOp] = {
		val overlayDir = new File(blog.srcdir, "overlay").getAbsoluteFile

		for {
			f <- files.find(overlayDir).filter(_.isFile)
		} yield (CopyFile(f, root.getAbsoluteFile.toPath.resolve(overlayDir.toPath.relativize(f.toPath)).toFile))
	}
}

object fishbowl extends BlogOps
