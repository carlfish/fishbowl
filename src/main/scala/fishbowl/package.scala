import scalaz.concurrent.Task
import scalaz.stream.Process

package object fishbowl {
  type Builder = (java.io.File, Blog) => Process[Task, FileOp]
}
