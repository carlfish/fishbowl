package fishbowl

import java.io.File
import scalaz._
import scalaz.stream._
import scalaz.concurrent.Task

trait IndexPageOps extends RenderOps {
	def rootPage = buildIndex(10, html.index(_, _).body, "index.html")
	def atomFeed = buildIndex(15, xml.atom(_, _).body, "atom.xml")

	def siteMap: Builder = (root, blog) =>
		Process.emit(WriteString(xml.sitemap(blog).body, new File(root, "sitemap.xml")))

	def buildIndex(postCount: Int,
		render: (Blog, List[(Post, String)]) => String,
		filename: String
	): Builder = (root, blog) => for {
		ps <- Process.emit(blog.posts.take(postCount))
		bs <- Process.eval(loadPosts(ps))
		body = render(blog, bs)
	} yield (WriteString(body, new File(root, filename)))
}

object indexPages extends IndexPageOps
