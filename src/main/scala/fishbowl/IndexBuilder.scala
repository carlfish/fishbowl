package fishbowl

import argonaut._, Argonaut._
import scalaz._
import scalaz.stream._
import scalaz.concurrent.Task
import scalaz.std.either._
import scalaz.syntax.std.either._
import scalaz.syntax.nel._
import scalaz.syntax.monad._
import java.io._;
import files._;

case class MTMetadata(
	author: String,
	title: String,
	basename: String,
	formatting: String,
	primaryCategory: Option[String],
	categories: Option[List[String]],
	excerpt: Option[String],
	publishDate: java.util.Date,
	tags: Option[List[String]],
	tag: Option[String]
	) {

	def toPost(basedir: File): Post =
		Post(title,
			author,
			basename,
			basedir,
			new File(basedir, "body.html"),
			Set() ++ primaryCategory.toList ++ categories.getOrElse(List()) ++ tags.getOrElse(List()),
			excerpt.flatMap(e => if (e.trim.length == 0) None else Some(e.trim)),
			publishDate.toInstant.atZone(java.time.ZoneId.of("Australia/Sydney")),
			tag)

}

trait IndexBuilderOps {
	implicit def defaultCodec = scala.io.Codec.UTF8
	def df: java.text.SimpleDateFormat = {
		val f = new java.text.SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX")
		f.setTimeZone(java.util.TimeZone.getTimeZone("UTC"))
		f
	}

	implicit def DateEncodeJson: EncodeJson[java.util.Date] = EncodeJson(d => jString(df.format(d)))
	implicit def DateDecodeJson: DecodeJson[java.util.Date] =
		optionDecoder(_.string flatMap (s => tryTo(df.parse(s))), "java.util.Date")

	def loadBlogConfig(f: File): Task[BlogConfig] = {
		implicit def AuthorConfigCodec: CodecJson[Author] =
			casecodec2(Author.apply, Author.unapply)("display-name", "uri")
		implicit def BlogConfigCodec: CodecJson[BlogConfig] =
			casecodec7(BlogConfig.apply, BlogConfig.unapply)("title", "longtitle", "subtitle", "description", "authors", "uri", "tag")

		loadFile(f).map(Parse.decodeEither[BlogConfig](_).disjunction.valueOr(error => throw new RuntimeException(f.getName + ": " + error)))
	}

	def loadMTMetadata(f: File): Task[String \/ Post] = {
		implicit def MTMetadataCodec: CodecJson[MTMetadata] =
			casecodec10(MTMetadata.apply, MTMetadata.unapply)(
				"author", "title", "basename", "formatting", "primaryCategory",
				"categories", "excerpt", "publishDate", "tags", "tag")

	    loadFile(f).map(Parse.decodeEither[MTMetadata](_).disjunction.map(_.toPost(f.getParentFile)))
	}

	def parseMTMetadata: File => Process[Task, String \/ Post] = f =>
		Process.eval(loadMTMetadata(f)).map(_.leftMap(f.getName + ": " + _))

	def reportErrors: String \/ Post => Task[Option[Post]] = res => Task { res match {
			case -\/(s) => println(s); None
			case \/-(p) => Some(p)
		}}

	def handleErrors: String \/ Post => Process[Task, Post] = result =>
		Process.eval(reportErrors(result)).pipe(Process.receive1[Option[Post], Post] { _ match {
			case Some(p) => Process.emit(p)
			case None => Process.empty
			}})

	 def loadBlogIndex(rootDirectory: File): Task[Blog] =
	 	find(rootDirectory)
	 		.filter(_.getName == "post.json")
	 		.flatMap(parseMTMetadata)
	 		.flatMap(handleErrors)
	 		.runLog
	 		.flatMap(posts =>
	 			loadBlogConfig(new java.io.File(rootDirectory, "blog-config.json")).map(new BlogIndex(rootDirectory, _, posts)))
}

object indexer extends IndexBuilderOps
