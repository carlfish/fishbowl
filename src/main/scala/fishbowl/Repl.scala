import fishbowl._
import scala.language.implicitConversions

object replHelper {
	lazy val src = sys.env("FISHBOWL_HOME")
	lazy val dest = sys.env("FISHBOWL_OUT")

	implicit def string2File(s: String) = new java.io.File(s)

	def build = fishbowl.build(src, dest)

	def blogIndex = indexer.loadBlogIndex(src)
}
