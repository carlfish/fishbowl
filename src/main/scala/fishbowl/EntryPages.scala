package fishbowl;

import java.io.File
import scalaz._
import scalaz.stream._
import scalaz.concurrent.Task

trait EntryPageOps extends RenderOps {
	def filesToCopy(root: File, blog: Blog, post: Post): Task[List[CopyFile]] = Task {
		val ff = new java.io.FilenameFilter {
			def accept(dir: File, name: String): Boolean = 
				!name.startsWith(".") && 
				!name.endsWith(".textile") && 
				!name.endsWith(".convert_linebreaks")
		}

		post.srcdir.listFiles(ff).toList.map(
			(f: File) => CopyFile(f, new File(blog.filePathOf(root, post), f.getName)))
	}

	def entryPages(root: File, blog: Blog): Process[Task, FileOp] = 
		Process.emitAll(blog.posts).flatMap(entryPage(root, blog, _))

	def entryPage(root: File, blog: Blog, post: Post): Process[Task, FileOp] =
		entryHtml(root, blog, post) ++ entryIncludedFiles(root, blog, post)

	def entryHtml(root: File, blog: Blog, post: Post): Process[Task, WriteString] = 
		Process.await(loadBody(post)) { body =>
			Process.emit(WriteString(
				html.post(post, body, blog).body, 
				new File(blog.filePathOf(root, post), "index.html")))
		}

	def entryIncludedFiles(root: File, blog: Blog, post: Post): Process[Task, CopyFile] = 
		Process.await(filesToCopy(root, blog, post))(Process.emitAll)
}

object entryPages extends EntryPageOps