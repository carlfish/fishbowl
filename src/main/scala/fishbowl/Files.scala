package fishbowl;

import scala.io.Source
import scalaz._
import scalaz.stream._
import scalaz.concurrent.Task
import scalaz.std.either._
import scalaz.syntax.nel._
import scalaz.syntax.std.option._
import scalaz.std.list._
import java.io._;
import java.nio.file.{Files, Paths, StandardCopyOption}
import java.nio.charset.{Charset, StandardCharsets}

sealed trait FileOp {
	def apply: Task[Unit]
}

case class WriteString(s: String, dest: File, charset: Charset = StandardCharsets.UTF_8) extends FileOp {
	def apply = files.writeFile(dest, s, charset)
}

case class CopyFile(src: File, dest: File) extends FileOp {
	def apply = files.copyFile(src, dest)
}

trait FileOps {
	// Because io.Source leaks open files
	def loadFile(f: File): Task[String] = Task {
			val is = new FileInputStream(f)
			val sb = new StringBuilder()
			val buf = new Array[Char](8096)

			try {
				val r = new InputStreamReader(is, "UTF-8")
				var n = 0;

				while (n != -1) {
					n = r.read(buf)
					if (n > 0) sb.appendAll(buf, 0, n)
				}

				sb.result
			} finally { is.close }
	}

	def perform(op: FileOp): Task[Unit] = op.apply

	def writeFile(dest: File, contents: String, charset: Charset): Task[Unit] = Task {
		println("Writing: " + dest.toString)
		dest.getParentFile.mkdirs
		Files.write(dest.toPath, contents.getBytes(charset))
	}

	def copyFile(src: File, dest: File): Task[Unit] = Task {
		println(s"Copying ${src.toString} to ${dest.toString}")
		dest.getParentFile.mkdirs
		Files.copy(src.toPath, dest.toPath, StandardCopyOption.REPLACE_EXISTING)
	}
}

trait FindOps {
	def find(f: File): Process[Task, File] = Process.await(startIn(f)) { root => fffind(root.wrapNel) }

	private def fffind(files: NonEmptyList[File]): Process[Task, File] = Process.await(explodeHead(files)) { fs =>
		Process.emit(fs.head) ++ fs.tail.toNel.some(fffind(_)).none(Process.empty)
	}

	private def explodeHead(files: NonEmptyList[File]): Task[NonEmptyList[File]] = {
		def subFiles(f: File): Task[List[File]] = Task { if (f.listFiles != null) List() ++ f.listFiles else List() }

		subFiles(files.head).map(fs => NonEmptyList.nel(files.head, IList.fromList(fs) ++ files.tail))
	}

	private def startIn(path: File): Task[File] = Task {
		if (!path.exists) throw new FileNotFoundException(path.toString)
		if (!path.isDirectory) throw new IOException("Not a directory: " + path.toString)
		path
	}
}


object files extends FindOps with FileOps
