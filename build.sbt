lazy val root = (project in file(".")).
  settings(
    name := "fishbowl",
    version := "0.1",
    scalaVersion := "2.11.12",
    scalacOptions ++= Seq("-unchecked", "-deprecation", "-feature"),
    mainClass in (run) := Some("fishbowl.App"),

    resolvers += "Scalaz Bintray Repo" at "http://dl.bintray.com/scalaz/releases",

    initialCommands in console := """import fishbowl._
        |import scalaz.concurrent.Task
        |import java.io.File
        |import java.time._
        |import replHelper._""".stripMargin,

    libraryDependencies ++= Seq(
    	"org.scalaz.stream" %% "scalaz-stream" % "0.8.4a",
    	"io.argonaut" %% "argonaut-scalaz" % "6.2-M1",

    	"org.specs2" %% "specs2-core" % "3.0.1" % "test",
    	"org.specs2" %% "specs2-scalacheck" % "3.0.1" % "test",
    	"org.scalacheck" % "scalacheck_2.11" % "1.12.2" % "test")
  ).enablePlugins(SbtTwirl)
